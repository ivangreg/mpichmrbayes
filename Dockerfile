## Use an official Fedora 32 runtime as a base image
FROM fedora:32
ENV container docker



# Author
LABEL maintainer="Ivan Gregoretti"



# Set the working directory to /opt
WORKDIR /opt



# Copy the current directory contents into the container at /opt
ADD . /opt



# Create users

RUN useradd -m -u 6799 -d /home/igregore -s /bin/bash -c "Ivan Gregoretti"  igregore && usermod -a -G wheel igregore && \
    useradd -m -u 6810 -d /home/sbrinton -s /bin/bash -c "Stephen Brinton"  sbrinton && usermod -a -G wheel sbrinton && \
    useradd -m -u 6812 -d /home/slandry  -s /bin/bash -c "Sean Landry"      slandry  && usermod -a -G wheel slandry  && \
    useradd -m -u 6829 -d /home/csimpson -s /bin/bash -c "Claire Simpson"   csimpson && usermod -a -G wheel csimpson && \
    useradd -m -u 6802 -d /home/ekolacz  -s /bin/bash -c "Elizabeth Kolacz" ekolacz  && \
    mkdir /home/igregore/Downloads && chown -R igregore:igregore /home/igregore/Downloads && \
    mkdir /home/igregore/.aws_enc  && chown -R igregore:igregore /home/igregore/.aws_enc  && \
    mkdir /home/igregore/.aws      && chown -R igregore:igregore /home/igregore/.aws      && \
    mkdir /scratch                 && chmod 0777                 /scratch



# Install packages

RUN sed -i -e "\$afastestmirror=true" /etc/dnf/dnf.conf && \
dnf install -y --nogpgcheck \
http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-32.noarch.rpm \
http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-32.noarch.rpm && \
dnf install -y dnf-plugins-core redhat-rpm-config && \
dnf clean all && \
\
\
dnf update -y && \
dnf --setopt=install_weak_deps=False install -y \
sudo vim exa htop nethogs bzip2 pbzip2 pigz findutils gcc-gfortran \
fuse-sshfs \
bash-completion \
gcc redhat-rpm-config \
automake fuse-devel gcc-c++ libcurl-devel libxml2-devel make openssl-devel \
\
\
EMBOSS \
python3-ipython python3-magic python3-PyQt4 python3-joblib python3-pandas \
\
\
xorg-x11-server-Xvfb \
\
\
hostname texlive-latex texlive-bibtex mpich mpich-devel && \
\
\
echo "############################################################" && \
echo "## Installation of Python packages via pip                ##" && \
echo "############################################################" && \
\
pip install dendropy ete3 niemads treeswift && \
\
\
echo "############################################################" && \
echo "## Installation of MrBayes                                ##" && \
echo "############################################################" && \
\
cd /opt/ && \
tar zxvf /opt/MrBayes-3.2.7a.tar.gz  && \
chown -R igregore:igregore /opt/MrBayes-3.2.7a* && \
runuser -l igregore -c 'cd /opt/MrBayes-3.2.7a; autoconf; PATH=/usr/lib64/mpich/bin:$PATH; ./configure --with-mpi --with-beagle=no; make' && \
cd /opt/MrBayes-3.2.7a && \
make install && \
\
\
echo "############################################################" && \
echo "## Installation of prank                                  ##" && \
echo "############################################################" && \
\
cd /opt && \
tar zxvf /opt/prank.linux64.170427.tgz && \
chown -R igregore:igregore /opt/prank* && \
\
\
echo "############################################################" && \
echo "## Installation of mafft                                  ##" && \
echo "############################################################" && \
\
cd /opt && \
tar zxvf /opt/mafft-7.470-without-extensions-src.tgz && \
cd /opt/mafft-7.470-without-extensions/core && \
sed --in-place \
    -e 's|PREFIX = /usr/local|PREFIX = /opt/mafft-7.470-without-extensions|' \
    -e 's|BINDIR = $(PREFIX)/bin|BINDIR = /opt/mafft-7.470-without-extensions/bin|' \
    -e 's|#ENABLE_MULTITHREAD|ENABLE_MULTITHREAD|' \
    /opt/mafft-7.470-without-extensions/core/Makefile && \
make clean && \
make && \
make install && \
chown -R igregore:igregore /opt/mafft-7.470-without-extensions* && \
\
\
echo "############################################################" && \
echo "## Installation of MMseqs2                                ##" && \
echo "############################################################" && \
\
cd /opt && \
tar zxvf mmseqs-linux-sse41.tar.gz && \
chown -R igregore:igregore /opt/mmseqs* && \
\
\
echo "############################################################" && \
echo "## Installation of TreeCluster                            ##" && \
echo "############################################################" && \
\
cd /opt && \
unzip TreeCluster-master.zip && \
chown -R igregore:igregore /opt/TreeCluster-master* && \
\
\
\
setcap "cap_net_admin,cap_net_raw+pe" /usr/sbin/nethogs && \
chmod 640 /etc/sudoers && sed --in-place -e 's|#\s*\(%wheel\s*ALL=(ALL)\s*NOPASSWD:\s*ALL\)|\1\nDefaults lecture=never|' /etc/sudoers && chmod 440 /etc/sudoers && \
sed --in-place -e 's|#\s*user_allow_other|user_allow_other|' /etc/fuse.conf



# MAFFT
# Very important: unpack, chown to igregore, and read the readme file. /opt/mafft-7.470-without-extensions/readme
# What it says is that for installation, we need to modify Makefile
# /opt/mafft-7.470-without-extensions/core/Makefile
# These are the only three modifications needed:
# 1) PREFIX = /usr/local
#    to
#    PREFIX = /opt/mafft-7.470-without-extensions
#
# 2) BINDIR = $(PREFIX)/bin
#    to
#    BINDIR = /opt/mafft-7.470-without-extensions/bin
#
# 3) To allow multithreading, uncomment line 8
#    ENABLE_MULTITHREAD = -Denablemultithread

# then
# cd /opt/mafft-7.470-without-extensions/core/
# make install
# test the installation (its a very long, single line)
#echo $'>Seq_1\nQEQLKESGGDLVKPEGSLTLTCTASGDSFSNYYYMCWVRQAPGKGLEWIACHYASSSGSTYYANWVNGRFTISKTSSTTVTLQMTSLTAADTATYFCAEDRIYATHIGYDYTTNDLWGPGTLVTVSS\n>Seq_2\nQSLEESGGRLVPPGTPLTLTCTVSGDSLNDYNMEWVRQAPGKGLEWIGSHTQSAIIYYANWAKGRFTISKTSTTVDLKMTSLTTEDTATYFCAEGVLTTIGNGNGIWGPGTLVTVSL\n>Seq_3\nQSLEESGGDLVKPGASLTLTCTPSGDSFNSAWICWVRQAPGKGLEWIACHYAGSSGGSDYASWAKGRFTVSKTSSTTVTLQMTSLTAADTATYFCAEDRIYATYINYDYTTNDLWGPGTLVTVSS\n>Seq_4\nQSLEESGGRLVPPGTPLTLTCTVSGDSLNDYNMEWVRQAPGKGLEWIGSHTQSGIIYYANWAKGRFTISKTSTTVDLKMTSLTTEDTATYFCAEGVLTTIGNGNGIWGPGTLVTVSL\n' | docker run --rm -i --log-driver=none -a stdin -a stdout -a stderr compbio-research/mpichmrbayes:0.4.0 mafft --amino --op 3 --ep 2 --maxiterate 10 --thread 2 --preservecase --inputorder --quiet --clustalout /dev/stdin | cat


 
# Re-set the working directory to /home
WORKDIR /home



# Setting up environment variables for whatever user runs the container
ENV PATH=/opt/prank/bin:$PATH
ENV PATH=/opt/mafft-7.470-without-extensions/bin:$PATH
ENV PATH=/opt/MrBayes-3.2.7a/src:$PATH
ENV PATH=/usr/lib64/mpich/bin:$PATH
ENV PATH=/opt/mmseqs/bin:$PATH
ENV PATH=/opt/TreeCluster-master:$PATH
# #echo $'# Added by Ivan\nPATH=$PATH:/usr/lib64/mpich/bin\nexport PATH\n' >> /etc/profile;
# #echo $'# Added by Ivan\nPATH=$PATH:/usr/lib64/mpich/bin\nexport PATH\n' >> /root/.bash_profile;
# ENV PATH "PATH=$PATH:/usr/lib64/mpich/bin"

