# MPI MrBayes
  
Author: Ivan Gregoretti, PhD.

This container is a minimal Fedora linux system hosting the phylogenetics
programme MrBayes compiled with support for parallel execution using MPICH.



## Building the container

    docker build -t compbio-research/mpichmrbayes:0.4.0 /base/my/script2/mpichmrbayes



## Using the container

### Using `mafft` from within the container

    $ docker run --rm \
    compbio-research/mpichmrbayes:0.4.0 mafft \
    --amino --op 3 --ep 2 --maxiterate 10 --thread 2 --preservecase --inputorder --quiet --clustalout /opt/test_sets/set1.fa
    
    CLUSTAL format alignment by MAFFT FFT-NS-i (v7.470)
    
    
    Seq_1           QEQLKESGGDLVKPEGSLTLTCTASGDSFSNYYYMCWVRQAPGKGLEWIACHYASSSGST
    Seq_2           Q-SLEESGGRLVPPGTPLTLTCTVSGDSL-NDYNMEWVRQAPGKGLEWIGSH--TQSAII
    Seq_3           Q-SLEESGGDLVKPGASLTLTCTPSGDSF-NSAWICWVRQAPGKGLEWIACHYAGSSGGS
    Seq_4           Q-SLEESGGRLVPPGTPLTLTCTVSGDSL-NDYNMEWVRQAPGKGLEWIGSH--TQSGII
                    * .*:**** ** *  .****** ****: *   : *************..*   .*.  
    
    Seq_1           YYANWVNGRFTISKTSSTTVTLQMTSLTAADTATYFCAEDRIYATHIGYDYTTNDLWGPG
    Seq_2           YYANWAKGRFTISKT-STTVDLKMTSLTTEDTATYFCAEGVLTTIGNG-----NGIWGPG
    Seq_3           DYASWAKGRFTVSKTSSTTVTLQMTSLTAADTATYFCAEDRIYATYINYDYTTNDLWGPG
    Seq_4           YYANWAKGRFTISKT-STTVDLKMTSLTTEDTATYFCAEGVLTTIGNG-----NGIWGPG
                     **.*.:****:*** **** *:*****: *********. : :   .     *.:****
    
    Seq_1           TLVTVSS
    Seq_2           TLVTVSL
    Seq_3           TLVTVSS
    Seq_4           TLVTVSL
                    ******

>Note: the default output format of `mafft` is FASTA and it is obtained by
omitting the `--clustalout` flag.



### Piping in and out of the container

    $ echo $'>Seq_1\nQEQLKESGGDLVKPEGSLTLTCTASGDSFSNYYYMCWVRQAPGKGLEWIACHYASSSGSTYYANWVNGRFTISKTSSTTVTLQMTSLTAADTATYFCAEDRIYATHIGYDYTTNDLWGPGTLVTVSS\n>Seq_2\nQSLEESGGRLVPPGTPLTLTCTVSGDSLNDYNMEWVRQAPGKGLEWIGSHTQSAIIYYANWAKGRFTISKTSTTVDLKMTSLTTEDTATYFCAEGVLTTIGNGNGIWGPGTLVTVSL\n>Seq_3\nQSLEESGGDLVKPGASLTLTCTPSGDSFNSAWICWVRQAPGKGLEWIACHYAGSSGGSDYASWAKGRFTVSKTSSTTVTLQMTSLTAADTATYFCAEDRIYATYINYDYTTNDLWGPGTLVTVSS\n>Seq_4\nQSLEESGGRLVPPGTPLTLTCTVSGDSLNDYNMEWVRQAPGKGLEWIGSHTQSGIIYYANWAKGRFTISKTSTTVDLKMTSLTTEDTATYFCAEGVLTTIGNGNGIWGPGTLVTVSL\n' \
    | docker run --rm \
    -i --log-driver=none -a stdin -a stdout -a stderr \
    compbio-research/mpichmrbayes:0.4.0 mafft \
    --amino --op 3 --ep 2 --maxiterate 10 --thread 2 --preservecase --inputorder --quiet --clustalout /dev/stdin \
    | cat
    
    CLUSTAL format alignment by MAFFT FFT-NS-i (v7.470)
    
    
    Seq_1           QEQLKESGGDLVKPEGSLTLTCTASGDSFSNYYYMCWVRQAPGKGLEWIACHYASSSGST
    Seq_2           Q-SLEESGGRLVPPGTPLTLTCTVSGDSL-NDYNMEWVRQAPGKGLEWIGSH--TQSAII
    Seq_3           Q-SLEESGGDLVKPGASLTLTCTPSGDSF-NSAWICWVRQAPGKGLEWIACHYAGSSGGS
    Seq_4           Q-SLEESGGRLVPPGTPLTLTCTVSGDSL-NDYNMEWVRQAPGKGLEWIGSH--TQSGII
                    * .*:**** ** *  .****** ****: *   : *************..*   .*.  
    
    Seq_1           YYANWVNGRFTISKTSSTTVTLQMTSLTAADTATYFCAEDRIYATHIGYDYTTNDLWGPG
    Seq_2           YYANWAKGRFTISKT-STTVDLKMTSLTTEDTATYFCAEGVLTTIGNG-----NGIWGPG
    Seq_3           DYASWAKGRFTVSKTSSTTVTLQMTSLTAADTATYFCAEDRIYATYINYDYTTNDLWGPG
    Seq_4           YYANWAKGRFTISKT-STTVDLKMTSLTTEDTATYFCAEGVLTTIGNG-----NGIWGPG
                     **.*.:****:*** **** *:*****: *********. : :   .     *.:****
    
    Seq_1           TLVTVSS
    Seq_2           TLVTVSL
    Seq_3           TLVTVSS
    Seq_4           TLVTVSL
                    ******

>Note: the `echo` command at the beginning and the `cat` command at the end are
outside the container. The docker arguments that make this possible are
`-i --log-driver=none -a stdin -a stdout -a stderr`.

The format of the output can be converted for example to Nexus using the EMBOSS
programme `seqret`. The following example shows how to perform that conversion
on the fly with a pipe.

    $ echo $'>Seq_1\nQEQLKESGGDLVKPEGSLTLTCTASGDSFSNYYYMCWVRQAPGKGLEWIACHYASSSGSTYYANWVNGRFTISKTSSTTVTLQMTSLTAADTATYFCAEDRIYATHIGYDYTTNDLWGPGTLVTVSS\n>Seq_2\nQSLEESGGRLVPPGTPLTLTCTVSGDSLNDYNMEWVRQAPGKGLEWIGSHTQSAIIYYANWAKGRFTISKTSTTVDLKMTSLTTEDTATYFCAEGVLTTIGNGNGIWGPGTLVTVSL\n>Seq_3\nQSLEESGGDLVKPGASLTLTCTPSGDSFNSAWICWVRQAPGKGLEWIACHYAGSSGGSDYASWAKGRFTVSKTSSTTVTLQMTSLTAADTATYFCAEDRIYATYINYDYTTNDLWGPGTLVTVSS\n>Seq_4\nQSLEESGGRLVPPGTPLTLTCTVSGDSLNDYNMEWVRQAPGKGLEWIGSHTQSGIIYYANWAKGRFTISKTSTTVDLKMTSLTTEDTATYFCAEGVLTTIGNGNGIWGPGTLVTVSL\n' \
    | docker run --rm \
    -i --log-driver=none -a stdin -a stdout -a stderr \
    compbio-research/mpichmrbayes:0.4.0 mafft \
    --amino --op 3 --ep 2 --maxiterate 10 --thread 2 --preservecase --inputorder --quiet --clustalout /dev/stdin \
    | docker run --rm \
    -i --log-driver=none -a stdin -a stdout -a stderr \
    compbio-research/mpichmrbayes:0.4.0 seqret \
    -osformat NEXUS /dev/stdin /dev/stdout \
    | cat

    Read and write (return) sequences
    #NEXUS
    [TITLE: Written by EMBOSS 06/07/20]

    begin data;
    dimensions ntax=4 nchar=127;
    format interleave datatype=protein missing=X gap=-;
    
    matrix
    Seq_1                QEQLKESGGDLVKPEGSLTLTCTASGDSFSNYYYMCWVRQAPGKGLEWIA
    Seq_2                Q-SLEESGGRLVPPGTPLTLTCTVSGDSL-NDYNMEWVRQAPGKGLEWIG
    Seq_3                Q-SLEESGGDLVKPGASLTLTCTPSGDSF-NSAWICWVRQAPGKGLEWIA
    Seq_4                Q-SLEESGGRLVPPGTPLTLTCTVSGDSL-NDYNMEWVRQAPGKGLEWIG
    
    Seq_1                CHYASSSGSTYYANWVNGRFTISKTSSTTVTLQMTSLTAADTATYFCAED
    Seq_2                SH--TQSAIIYYANWAKGRFTISKT-STTVDLKMTSLTTEDTATYFCAEG
    Seq_3                CHYAGSSGGSDYASWAKGRFTVSKTSSTTVTLQMTSLTAADTATYFCAED
    Seq_4                SH--TQSGIIYYANWAKGRFTISKT-STTVDLKMTSLTTEDTATYFCAEG
    
    Seq_1                RIYATHIGYDYTTNDLWGPGTLVTVSS
    Seq_2                VLTTIGNG-----NGIWGPGTLVTVSL
    Seq_3                RIYATYINYDYTTNDLWGPGTLVTVSS
    Seq_4                VLTTIGNG-----NGIWGPGTLVTVSL
    ;
    
    end;
    begin assumptions;
    options deftype=unord;
    end;



### Using `prank`

This programme can perform codon-based multiple sequence alignment.

    docker run --rm \
    -v /tmp:/tmp \
    -u igregore:igregore -w /home/igregore \
    compbio-research/mpichmrbayes:0.4.0 prank \
    -codon -termgap -iterate=7 -d=/opt/test_sets/set2.fa -f=nexus -o=/tmp/set2_aln

The command above generates a Nexus file named `/tmp/set2_aln.best.nex`.

    $ exa -l /tmp/set2_aln*

    .rw-r--r--@ 22k igregore 16 Jul 13:18 /tmp/set2_aln.best.nex

That output seems to be non-standard because MrBayes throws errors and quits
when trying to parse it. A more robust alternative is to output in FASTA format.
In this example the output file would be `/tmp/set2_aln.best.fas`.

    docker run --rm \
    -v /tmp:/tmp \
    -u igregore:igregore -w /home/igregore \
    compbio-research/mpichmrbayes:0.4.0 prank \
    -codon -termgap -iterate=7 -d=/opt/test_sets/set2.fa -f=fasta -o=/tmp/set2_aln

    $ exa -l /tmp/set2_aln*

    .rw-r--r--@ 14k igregore 16 Jul 13:21 /tmp/set2_aln.best.fas

If a standard Nexus file is needed to feed MrBayes, the aligned FASTA can be
converted using `seqret`.

    cat /tmp/set2_aln.best.fas \
    | docker run --rm \
    -i --log-driver=none -a stdin -a stdout -a stderr \
    compbio-research/mpichmrbayes:0.4.0 seqret -osformat NEXUS /dev/stdin /dev/stdout \
    > /tmp/set2_aln.nex



### Using MrBayes with parallel execution capabilities

MrBayes needs the value passed to mpirun's `-np` flag to be either 8, 4, 2 or 1.

The following instructions

    $ echo $'set Autoclose=yes Nowarn=yes;\nexecute /opt/test_sets/set2_aln.nex;\nlset Nst=mixed;\nmcmcp Samplefreq=1000 Printfreq=1000;\nmcmc Ngen=500000 Filename=/tmp/set2_aln.mcmc;\nsumt Conformat=simple;\nquit;'

    set Autoclose=yes Nowarn=yes;
    execute /opt/test_sets/set2_aln.nex;
    lset Nst=mixed;
    mcmcp Samplefreq=1000 Printfreq=1000;
    mcmc Ngen=500000 Filename=/tmp/set2_aln.mcmc;
    quit;

tell MrBayes to take input file `/opt/test_sets/set2_aln.nex` (included
in this container) and produce a group of output files with the prefix
`/tmp/set2_aln.mcmc`.

It is not simple to pipe MrBayes' commands into the MPI instruction.
The process is then better split into two steps.

    # Step 1. Write MrBayes' commands to an intermediate file.
    echo $'set Autoclose=yes Nowarn=yes Seed=12345 Swapseed=67890;\nexecute /opt/test_sets/set2_aln.nex;\nlset Nst=mixed;\nmcmcp Samplefreq=1000 Printfreq=1000;\nmcmc Ngen=500000 Filename=/tmp/set2_aln.mcmc;\nsumt Conformat=simple;\nquit;' \
    > /tmp/set2_aln.txt

    # Step 2. Instruct MPICH to run MrBayes.
    docker run --rm \
    -v /tmp:/tmp \
    -u igregore:igregore \
    compbio-research/mpichmrbayes:0.4.0 mpirun -np 8 mb /tmp/set2_aln.txt \
    > /tmp/set2_aln.mcmc.log

>Note: this two-step approach requires the creation of the intermediate file
`/tmp/set2_aln.txt`.

If piping is necesary, this is the single step alternative

    echo $'set Autoclose=yes Nowarn=yes Seed=12345 Swapseed=67890;\nexecute /opt/test_sets/set2_aln.nex;\nlset Nst=mixed;\nmcmcp Samplefreq=1000 Printfreq=1000;\nmcmc Ngen=500000 Filename=/tmp/set2_aln.mcmc;\nsumt Conformat=simple;\nquit;' \
    | docker run --rm \
    -i --log-driver=none -a stdin -a stdout -a stderr \
    -v /tmp:/tmp \
    -u igregore:igregore \
    compbio-research/mpichmrbayes:0.4.0 mpirun -np 8 mb < /dev/stdin \
    > /tmp/set2_aln.mcmc.log

When the execution is done, the output files can be seen in their specified location

    $ exa -l /tmp/set2_aln.mcmc*

    .rw-r--r--@  25k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.ckp
    .rw-r--r--@  25k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.ckp~
    .rw-r--r--@ 3.6k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.con.tre
    .rw-rw-r--@  87k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.log
    .rw-r--r--@  53k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.mcmc
    .rw-r--r--@ 5.8k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.parts
    .rw-r--r--@  91k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.run1.p
    .rw-r--r--@ 764k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.run1.t
    .rw-r--r--@  91k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.run2.p
    .rw-r--r--@ 764k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.run2.t
    .rw-r--r--@ 221k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.trprobs
    .rw-r--r--@ 3.5k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.tstat
    .rw-r--r--@ 9.7k igregore 16 Jul 16:11 /tmp/set2_aln.mcmc.vstat

>Note: the consensus tree file (`.con.tre`) is generated by passing to MrBayes
the instructions `sumt Conformat=simple`.



### Creating graphical output of phylogenetic relationships

The following examples show a programmatic way of creating graphical
representations of phylogeny. The solution shown here requires the creation of
a Newick (that is created from a Nexus file) and a multiple sequence alignment
in FASTA format. The final output are graphic files such us SVG, PNG, and PDF.


#### Performing a DNA to aa translation of a gapped FASTA file

    docker run --rm \
    -v /tmp:/tmp \
    -u igregore:igregore -w /home/igregore \
    compbio-research/mpichmrbayes:0.4.0 prank \
    -convert -translate -d=/opt/test_sets/set2_aln.nex -f=fasta -keep -o=/tmp/set2_aln_aa

>Note: the flag `-keep` instructs `prank` to retain the gaps in the alignment
during the format conversion.

>Note: the `-o=/tmp/set2_aln_aa` directive will create
`/tmp/set2_aln_aa.fas`.

    $ head -n 12 /tmp/set2_aln_aa.fas

    >41_6_10
    CAGAPYYIFGYI---GY------A-----------------------------V------
    -------G-------GY-----N-------FW
    >56_59_3
    CAR------------GS------A-----------------------------S------
    -------G-------AY-----N-------FW
    >27_62_3
    CAR----------EPGS------S-----------------------------Y------
    -------G-------AV-----N-------LW
    >31_56_3
    CAR---------VEAGT------G-----------------------------S------
    -------G--------F-----N-------LW



#### Creating Newick formatted files with Python's `dendropy` package

MrBayes can create a consensus tree file (`.con.tre`) which is a Nexus file.
The two consesnsus trees here named `a` and `b` can be extrated to Newick format
files with `dendropy`.

    # Tree a
    cat /tmp/set2_aln.mcmc.con.tre \
    | docker run --rm \
    -i --log-driver=none -a stdin -a stdout -a stderr \
    -v /tmp:/tmp \
    -u igregore:igregore \
    compbio-research/mpichmrbayes:0.4.0 python \
    -c 'import sys, dendropy; treelist=dendropy.TreeList.get(file=sys.stdin, schema="nexus"); treelist[0].write(file=sys.stdout,schema="newick")' \
    > /tmp/set2_aln.mcmc.con.a.nw    

    # Tree b
    cat /tmp/set2_aln.mcmc.con.tre \
    | docker run --rm \
    -i --log-driver=none -a stdin -a stdout -a stderr \
    -v /tmp:/tmp \
    -u igregore:igregore \
    compbio-research/mpichmrbayes:0.4.0 python \
    -c 'import sys, dendropy; treelist=dendropy.TreeList.get(file=sys.stdin, schema="nexus"); treelist[1].write(file=sys.stdout,schema="newick")' \
    > /tmp/set2_aln.mcmc.con.b.nw    



#### Creating graphical representations of consensus trees with the ETE toolkit

ETE can create a graph files like .svg but to do so at the command line without
GUI it needs to be tricked. The solution is to use xvfb-run provided by the
package xorg-x11-server-Xvfb.

Trees can be graphed to SVG, PDF or PNG.

    # Tree a
    docker run --rm \
    -v /tmp:/tmp \
    -u igregore:igregore \
    compbio-research/mpichmrbayes:0.4.0 xvfb-run ete3 view \
    --Ir 300 -mbs 1 --ss --face 'value:@name, size:8, pos:b-right' -t /opt/test_sets/set2_aln.mcmc.con.a.nw \
    --alg_type 'fullseq' --alg /opt/test_sets/set2_aln_aa.fas \
    --image /tmp/set2_aln.mcmc.con.a.nw.pdf

    # Clean up
    rm -f /tmp/.X99-lock


    # Tree b
    docker run --rm \
    -v /tmp:/tmp \
    -u igregore:igregore \
    compbio-research/mpichmrbayes:0.4.0 xvfb-run ete3 view \
    --Ir 300 -mbs 1 --ss --face 'value:@name, size:8, pos:b-right' -t /opt/test_sets/set2_aln.mcmc.con.b.nw \
    --alg_type 'fullseq' --alg /opt/test_sets/set2_aln_aa.fas \
    --image /tmp/set2_aln.mcmc.con.b.nw.pdf

    # Clean up
    rm -f /tmp/.X99-lock

>Important note: the command above leaves behind a file that prevents re-running
`xvfb-run` again. To solve this problem, be sure to execute
`rm -f /tmp/.X99-lock`. This would not be a problem if the /tmp directory was
not shared between host and container.



### Clustering of sequences with MMseqs2

MMseqs2 can cluster sequences not by performing a mutiple sequence alignment but
by computing all pairwise alignments. The value of the argument`--min-seq-id`
must be choosen by testing multiple values and assesing what works best for the
problem at hand. MMseqs2 clusters sequences without the need to infer their
phylogenetic relationships.

    docker run --rm -v /tmp:/tmp -u igregore compbio-research/mpichmrbayes:0.4.0 mmseqs easy-cluster \
    /opt/test_sets/set3.fa.gz /tmp/set3_mmseqs /scratch \
    --min-seq-id 0.85 --cluster-mode 0 --gap-open 3 --gap-extend 2 --dbtype 2 --threads 8

The command above generates three files

    $ exa -l /tmp/set3_mmseqs*

    .rw-r--r--@  39k igregore  6 Aug 16:29 /tmp/set3_mmseqs_all_seqs.fasta
    .rw-r--r--@ 8.0k igregore  6 Aug 16:29 /tmp/set3_mmseqs_cluster.tsv
    .rw-r--r--@  21k igregore  6 Aug 16:29 /tmp/set3_mmseqs_rep_seq.fasta

Cluster identification in tabular format is written to a headerless `.tsv` file

    $ csvlook --tabs --no-header-row --no-inference /tmp/set3_mmseqs_cluster.tsv

    | a          | b          |
    | ---------- | ---------- |
    | 316_0_192  | 316_0_192  |
    | 50_1_186   | 50_1_186   |
    | 282_2_165  | 282_2_165  |
    | 282_2_165  | 282_224_9  |
    | 282_2_165  | 282_243_8  |
    | 221_3_151  | 221_3_151  |
    | 118_8_119  | 118_8_119  |
    | 191_9_117  | 191_9_117  |
    | 281_10_112 | 281_10_112 |
    | 188_11_112 | 188_11_112 |
    | 188_11_112 | 188_41_51  |
    | 188_11_112 | 188_49_40  |
    | 188_11_112 | 188_268_7  |
    | 238_12_106 | 238_12_106 |
    (truncated output)

The example shown above consists of 405 sequences and the clsutering takes about
five seconds of computing time.



### Clustering of sequences with TreeCluster.py

TreeCluster takes phylogenetic trees in Newick format.

The example shown below consists of a tree made up of 502 taxa. The tabular
output is truncated to display the first handful of taxon names. The threshold
value of 0.045 is suggested by the creators of TreeCluster based on their own
application of the software.

    $ docker run --rm compbio-research/mpichmrbayes:0.4.0 TreeCluster.py \
    -i /opt/test_sets/set4_aln.mcmc.con.a.nw -o /dev/stdout --threshold 0.045 \
    | csvlook --tabs --no-inference

    | SequenceName | ClusterNumber |
    | ------------ | ------------- |
    | 337_0_304    | -1            |
    | 337_60_28    | -1            |
    | 337_23_72    | -1            |
    | 337_375_4    | -1            |
    | 114_407_4    | -1            |
    | 166_398_4    | -1            |
    | 252_391_4    | -1            |
    | 252_501_3    | -1            |
    | 388_429_4    | -1            |
    | 7_411_4      | -1            |
    | 402_554_3    | -1            |
    | 377_14_119   | -1            |
    | 71_58_28     | -1            |
    | 209_324_5    | -1            |
    | 15_248_7     | -1            |
    | 88_558_3     | 1             |
    | 88_500_3     | 1             |
    | 15_43_39     | -1            |
    | 352_523_3    | -1            |
    | 206_78_23    | -1            |
    | 261_312_5    | -1            |
    | 278_219_8    | -1            |
    | 373_241_7    | -1            |
    | 123_376_4    | -1            |
    | 123_518_3    | 2             |
    | 123_50_32    | 2             |
    | 31_68_26     | -1            |
    (truncated output)

Sequences assigned cluster number -1 are ungroup and therefore they are the sole
members of their own cluster (singletons).

The result above shows mostly singletons with the exception of cluster 1 (taxa
`88_558_3` and `88_500_3`) and cluster 2 (taxa `123_518_3`, `123_50_32`).


.//
